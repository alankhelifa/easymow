import org.scalatest._

class FieldSpec extends FlatSpec with Matchers {
  "Is inside" should "be true" in {
    val f = RectangularField(5,5)
    assert(f.isInside(Position(1,1)) === true)
    assert(f.isInside(Position(5,5)) === true)
    assert(f.isInside(Position(0,0)) === true)
  }
  it should "be false" in {
    val f = RectangularField(3,3)
    assert(f.isInside(Position(6,1)) === false)
    assert(f.isInside(Position(-1,1)) === false)
  }


  "From String" should "create a Field" in {
    assert(RectangularField.fromString("10 10") === Some(RectangularField(10,10)))
  }
  it should "create nothing" in {
    assert(RectangularField.fromString("6 8 5") === None)
    assert(RectangularField.fromString("10") === None)
  }
}
