import org.scalatest._

class MowerSpec extends FlatSpec with Matchers {

  "From String" should "create a Mower" in {
    assert(Mower.fromString("0 0 N") === Some(Mower(Position(0, 0), Direction.North)))
    assert(Mower.fromString("1 1 X") === Some(Mower(Position(1, 1), Direction.Unrecognized)))
  }
  it should "create nothing" in {
    assert(Mower.fromString("0 0 N c") === None)
    assert(Mower.fromString("0 S") === None)
  }

  "processCommand" should "move the mower" in {
    val mower = Mower(Position(0, 0), Direction.East)
    val field = RectangularField(4, 4)
    assert(mower.processCommand(Forward).print() === "1 0 E")
    assert(mower.processCommand(Left).processCommand(Forward).print() === "0 1 N")
    assert(mower.processCommand(Right).processCommand(Forward).print() === "0 -1 S")
    assert(field.isInside(mower.processCommand(Right).processCommand(Forward).position) === false)
  }
  "processCommand" should "not move the mower" in {
    val mower = Mower(Position(2, 2), Direction.East)
    assert(mower.processCommand(Stasis).print() === "2 2 E")
  }

}