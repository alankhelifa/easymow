import org.scalatest._

class CommandSpec extends FlatSpec with Matchers {

  "translate" should "return Forward, Left, Right" in {
    assert(Command.translate('A') === Forward)
    assert(Command.translate('G') === Left)
    assert(Command.translate('D') === Right)
  }
  it should "return stasis" in {
    "BCEFHIJKLMNOPQRSTUVWXYZ ! w<".foreach(c => assert(Command.translate(c) === Stasis))
  }

}