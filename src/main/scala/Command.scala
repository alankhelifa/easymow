sealed trait Command
object Left extends Command
object Right extends Command
object Forward extends Command
object Stasis extends Command

object Command {

  def translate (command: Char) : Command = {
    command match {
      case 'G' => Left
      case 'D' => Right
      case 'A' => Forward
      case _ => Stasis
    }
  }
}