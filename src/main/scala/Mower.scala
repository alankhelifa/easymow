import Direction._

case class Mower (position: Position, direction: Direction){
  def forward () : Mower = {
    direction match {
      case North => this.copy(position = Position(position.x, position.y + 1))
      case East => this.copy(position = Position(position.x + 1, position.y))
      case South => this.copy(position = Position(position.x, position.y - 1))
      case West => this.copy(position = Position(position.x - 1, position.y))
      case Unrecognized => this
    }
  }

  def turn(command: Command) : Mower = {
    command match {
      case Left => this.copy(direction = direction.left)
      case Right => this.copy(direction = direction.right)
      case _ => this
    }
  }

  def processCommand (command: Command) : Mower = {
    command match {
      case Forward => this.forward()
      case Left | Right => this.turn(command)
      case Stasis => this
    }
  }

  def print() : String = {
    Array(this.position.x, this.position.y, this.direction.print).mkString(" ")
  }
}

object Mower {
  def fromString(line: String) : Option[Mower] = {
    line.split(" ") match {
      case Array(x, y, d) => Some(Mower(Position(x.toInt, y.toInt), Direction.translate(d)))
      case _ => None
    }
  }

  def mow(field: Field, mower: String, moveSet: String) : Option[String] = {
    fromString(mower) match {
      case Some(m) =>
        Some(moveSet.foldLeft(m) {
          case (_mower, command) =>
            val next = _mower.processCommand(Command.translate(command))
            if (field.isInside(next.position)) next else _mower
        }.print())
      case None => None
    }
  }
}
