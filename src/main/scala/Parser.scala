import scala.io.Source.fromInputStream

object Parser {
  def readFile(file: String) : List[String] = {
    Option(getClass.getResourceAsStream(file)) match {
      case None => Nil

      case Some(stream) =>
        val source = fromInputStream(stream)
        val lines = try source.getLines.toList finally source.close()
        lines
    }
  }
}
