import scala.util.{Failure, Success, Try}

object Main extends App {

  def error(message: String): Unit = {
    message match {
      case "" | null => println("error in file")
      case _ => println(message)
    }
    System.exit(1)
  }

  def process(field: Field, lines: List[String]) : Any = {
    lines match {
      case mower :: moveSet :: rest => {
        Mower.mow(field, mower, moveSet) match {
          case Some(finalPosition) => println(finalPosition)
          case None => error("Malformed mower description")
        }
        process(field, rest)
      }
      case Nil => ()
      case _ => error("Malformed file")
    }
  }

  def start(lines: List[String]) : Unit = {
    RectangularField.fromString(lines.head) match {
      case Some(f) => process(f, lines.drop(1))
      case None => System.exit(1)
    }
  }

  val file = "moves.txt"
  Parser.readFile(file) match {
      case Nil => error("Could not find file")
      case list => start(list)
  }
}
