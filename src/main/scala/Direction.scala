sealed trait Direction {
  def right: Direction
  def left: Direction
  def print: String
}

object Direction {

  case object North extends Direction {
    override def right = East
    override def left = West
    override def print: String = "N"
  }

  case object East extends Direction {
    override def right = South
    override def left = North
    override def print: String = "E"
  }

  case object South extends Direction {
    override def right = West
    override def left = East
    override def print: String = "S"
  }

  case object West extends Direction {
    override def right = North
    override def left = South
    override def print: String = "W"
  }

  case object Unrecognized extends Direction {
    override def left = Unrecognized
    override def right= Unrecognized
    override def print: String = "?"
  }

  def translate (direction: String) : Direction = {
    direction match {
      case "N" => North
      case "E" => East
      case "S" => South
      case "W" => West
      case _ => Unrecognized
    }
  }
}