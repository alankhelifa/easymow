trait Field {
  def isInside(p: Position): Boolean
}

case class RectangularField(x: Int, y: Int) extends Field {

  override def isInside(position: Position) : Boolean = {
    position.x >= 0 && position.y >= 0 && position.y <= y && position.x <= x
  }
}

object RectangularField {
  def fromString(line: String) : Option[Field] = {
    line.split(" ") match {
      case Array(x, y) => Some(RectangularField(x.toInt, y.toInt))
      case _ => None
    }
  }
}