# EsayMow
EasyMow is a simple scala project representing mowers mowing a field aimed to works on the basics of Scala and more generally functionnal programming

## Building and executing
EasyMow can be built and run with **sbt**

To launch the tests:
```
sbt test
```

to run the program:
```
sbt run
```

A single resource file can be modified to give the mowers instruction located in 
**src > main > resources > moves.txt**

## Details
I started implementing the different commands that my mowers could later use with a simple Sealed trait.
```scala
sealed trait Command  
object Left extends Command  
object Right extends Command  
object Forward extends Command  
object Stasis extends Command
```
Stasis is let the mower stay in place when a command is not recognized.

Mowers are represented by a position and a direction.
The field represents the area to mow.

When receiving commands the mowers are able to move on the field if they stay within its limits or else they stay in place.
